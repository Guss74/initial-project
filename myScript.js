//window.alert("Welcome to\nJavaScript\nProgramming")

function getAgeRating(Age)
{
if (Age<8) 
	return 0;
else if (Age<12 && Age>=8)
	return 8;
else if (Age<16 && Age>=12)
	return 12;
else if (Age<18 && Age>=16)
	return 16;
else
	return 18;	
}

var movies = [{title:"The Lord of the Rings", rating:16}, 
		{title:"Frozen", rating:8},
		{title:"Toy Story", rating:0},
		{title:"Pulp Fiction", rating:18},
		{title:"Spiderman", rating:8},
		{title:"Godfather", rating:18},
		{title:"Men in Black", rating:12},
		{title:"Avengers", rating:16},
		{title:"Minions", rating:0},
		{title:"Billy Elliot", rating:12}];

do {
var userAge = prompt("Enter your age", "0" );
} while (isNaN(userAge));

var allowedrating = getAgeRating(userAge);

var AllowedMovies =  "";

for (var i=0; i<movies.length; i++) {
	if (movies[i].rating<=allowedrating) {AllowedMovies = AllowedMovies + movies[i].title + "\n"};
};

alert("You can watch the following movies:\n" + AllowedMovies);

/*
alert("You can watch K-" + getAgeRating(userAge) + " rated movies")

//alert("Your age is: " + userAge + " " + typeof(userAge));

var now = new Date();

window.alert("Current date and time: " + now.toLocaleString());

*/