function validateForm(){
	var name = document.getElementById('name').value;
	var age = document.getElementById('age').value;
	var tk = document.getElementById('tk').value;
	var alert1 = "Please fill the required fields correctly";
	var alert2 = "Please enter a 5 digit tk";
	var response = "";
	var allowedRating;
	var movies = [{title:"The Lord of the Rings", rating:16}, 
		{title:"Frozen", rating:8},
		{title:"Toy Story", rating:0},
		{title:"Pulp Fiction", rating:18},
		{title:"Spiderman", rating:8},
		{title:"Godfather", rating:18},
		{title:"Men in Black", rating:12},
		{title:"Avengers", rating:16},
		{title:"Minions", rating:0},
		{title:"Billy Elliot", rating:12}];	
	
	if (name == "" || age == "" || isNaN(age)){
		response = response + alert1 + "\n";
	}
	
	if (tk.length!=5 ||isNaN(parseInt(tk))){
		response = response + alert2 + "\n";
	}
	
	if (response!=""){
		alert(response)
	}
	else{
		allowedRating = getAgeRating(age);
		var resultsHeading = document.createElement("h2");
		var headingText = document.createTextNode("You can watch K" + allowedRating +" rated movies")
		resultsHeading.appendChild(headingText);
		var bodyElement = document.getElementsByTagName("BODY")[0];
		bodyElement.appendChild(resultsHeading);
		var ulElement = document.createElement("ul");	
		for (i=0; i<movies.length; i++) {
			if (movies[i].rating <= allowedRating) {			
				var liElement = document.createElement("li");
				var liText = document.createTextNode(movies[i].title);
				liElement.appendChild(liText);
				ulElement.appendChild(liElement);			
			}			
		}
		bodyElement.appendChild(ulElement);
	}
	return false;	
}

function getAgeRating(Age){
	if (Age<8) 
		return 0;
	else if (Age<12 && Age>=8)
		return 8;
	else if (Age<16 && Age>=12)
		return 12;
	else if (Age<18 && Age>=16)
		return 16;
	else
		return 18;	
}

function greeting(){
	if (document.watchMovies.gender[1].checked)
		alert("So you are a man!");
	if (document.watchMovies.gender[0].checked)
		alert("So you are a woman!");
	// bad style!
var paragraph = document.getElementById("welcome");
paragraph.innerHTML = "<p>text and <a href=\"page.html\">link</a></p>";   
}
